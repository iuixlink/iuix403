<?php

namespace Modules\Users\Controllers;

use App\Http\Controller;
use Modules\Users\Models\Tag;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function getTags(Request $request)
    {
        $user = $request->user();        
        $tags = Tag::where('userId', $user->id)->get();
        return $this->response($tags);
    }

    public function getTag(Request $request, $id)
    {
        $tag = Tag::find($id);
        $user = $request->user();        
        
        if(!$tag) {
            return response(['error' => 'notFound'], 422);
        }

        if ($user->id == $tag->userId) {
            return $this->response($tag);
        } else {
            return response(['error' => 'noTuTag'], 422);
        }

    }

    public function updateTag(Request $request, $id)
    {
        $tag = Tag::find($id);
        $user = $request->user();        

        if(!$tag) {
            return response(['error' => 'notFound'], 422);
        }

        if ($user->id == $tag->userId) {
            $tag->update(['url' => $request->input("url")]);
            return $this->response($tag);
        } else {
            return response(['error' => 'noTuTag'], 422);
        }
    }
}
