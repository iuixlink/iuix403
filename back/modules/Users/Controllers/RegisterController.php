<?php

namespace Modules\Users\Controllers;

use App\Http\Controller;
use Modules\Users\Models\User;
use Modules\Users\Requests\RegisterRequest;
use Modules\PublicTags\Models\Tag;
use Illuminate\Support\Facades\Auth;

class RegisterController extends Controller
{
    /**
     * Store a new user.
     *
     * @param  RegisterRequest  $request
     * @return JSON
     */
    public function register(RegisterRequest $request)
    {
        $requestTag = $request->input("tag");
        $tagId = substr($requestTag, 0, -3);
        $tagPass = substr($requestTag, 7, 10);

        $tag = Tag::find($tagId);

        if(!$tag) {
            return response(['error' => 'tagNotFound'], 422);
        } else {
            if($tag->pass == $tagPass) {
                if($tag->configured) {
                return response(['error' => 'tagAlreadyConfigured'], 422);
                }
                // Save to DB
                $user = new User($request->all());
                $user->save();

                $tag->update(['userId' => $user->id]);
                $tag->update(['url' => "https://twitter.com/soyancapman/status/1336696277132828674"]);
                $tag->update(['enabled' => true]);                
                $tag->update(['configured' => true]);                

                $credentials = $request->only(['email', 'password']);
                $token = Auth::attempt($credentials);
                $data = [
                    'token' => $token,
                    'token_type' => 'bearer',
                    'expires_in' => Auth::factory()->getTTL() * 60
                ];
                $user = Auth::user()->toArray();
                $data = array_merge($data, $user);

                // Final Response
                return $this->response($data);

                // Final Response
                //return $this->response(['message' => __('general_words.process_success')], 201);
            } else {
                return response(['error' => 'invalidTag'], 422);
            }
        }
    }
}
