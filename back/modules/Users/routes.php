<?php

/** @var \Laravel\Lumen\Routing\Router $router */
$router = $this->app['router'];

// Auth
$router->post('/login', 'LoginController@login');
$router->post('/register', 'RegisterController@register');

$router->get('/tags', [
    'middleware' => 'auth',
    'uses' => 'UserController@getTags'
]);

$router->get('/tags/{id}', [
    'middleware' => 'auth',
    'uses' => 'UserController@getTag'
]);


$router->put('/tags/{id}', [
    'middleware' => 'auth',
    'uses' => 'UserController@updateTag'
]);

// Users
$router->group(['middleware' => 'auth'], function () use ($router) {
    //$router->get('/tags', 'UserController@getTags');

    $router->get('/users/{id:[0-9]+}', 'UserController@get');
    $router->post('/users', 'UserController@create');
    $router->put('/users', 'UserController@update');
    $router->delete('/users', 'UserController@delete');
});
