<?php

namespace Modules\PublicTags\Providers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\ServiceProvider;

class PublicTagsServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Route::group([
            'prefix' => '',
            'namespace' => 'Modules\PublicTags\Controllers',
            'middleware' => null,
        ], function () {
            $this->loadRoutesFrom(__DIR__. '/../routes.php');
        });
    }
}
