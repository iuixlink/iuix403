<?php

/** @var \Laravel\Lumen\Routing\Router $router */
$router = $this->app['router'];

$router->get('/{id}', 'TagController@get');
