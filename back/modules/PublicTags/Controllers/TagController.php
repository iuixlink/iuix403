<?php

namespace Modules\PublicTags\Controllers;

use App\Http\Controller;
use Modules\PublicTags\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class TagController extends Controller
{
    /**
     * Get a tag
     *
     * @param  id  $id
     * @return JSON
     */
    public function get(Request $request, $id)
    {
        $tag = Tag::find($id);
        
        if (!$tag) {
            return RedirectResponse::create("https://iuix.link");
        } else {
            if ($tag->configured && $tag->enabled && $tag->url) {
                return RedirectResponse::create($tag->url);
            } else {
                if ($request->input('p') != $tag->pass) {
                    return RedirectResponse::create("https://iuix.link");
                }
                if($tag->configured) {
                    return RedirectResponse::create("https://app.iuix.link/login/".$tag->id."?tagPass=".$tag->pass);
                }
                return RedirectResponse::create("https://app.iuix.link/register/".$tag->id."?tagPass=".$tag->pass);
            }

        }
    }
}
