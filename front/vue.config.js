var path = require('path')
module.exports = {
    pluginOptions: {
        i18n: {
            locale: 'es',
            fallbackLocale: 'es',
            localeDir: 'locales',
            enableInSFC: false
        }
    }
}