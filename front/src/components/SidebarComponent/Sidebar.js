export default {
    name: 'Sidebar-Component',
    props: ['activeMenu'],
    methods: {
        logout: function() {
            this.$store.dispatch('logOut')
			this.$router.push({ name: 'login' })
        },
    }
}