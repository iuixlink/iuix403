import QrcodeVue from 'qrcode.vue'
import httpAxios from '@/httpAxios.js'

export default {
    name: 'Tag-Module',
	metaInfo: function() {
		return {
			title: this.$t('navbar.dashboard'),
		}
	},
	data: () => {
		return { 
			tag: {},
			errors: {
				url: false
			}
		}
	}, 
	methods: {
		async fetchTags() {
			const response = await httpAxios({
				url: '/tags/' + this.$route.params.id,
				method: 'GET'
			});
			this.tag = response.data;
		},
		updateUrl: function() {
            const self = this

            // Clear Errors
            Object.keys(self.errors).forEach(function(key) {
                self.errors[key] = false
            })
            
            // Ajax Request
            httpAxios({
                url: '/tags/' + this.$route.params.id,
                method: 'PUT',
                data: {url : self.tag.newUrl}
            })
			.then(response => {
                self.$notify({
                    group: 'notify',
                    type: 'success',
                    title: self.$t('general.success'),
                    text: 'The process was successfully completed'
                })
                self.tag.url = self.tag.newUrl;
                console.log(response)
            })
            .catch(error => {
                try {
                    console.log(error)
					if(error.response.status == 422) {
						for(var errorKey in error.response.data.errors) {
							if(errorKey in self.errors) {
                                self.errors[errorKey] = true
                            }
						}
					}
				} catch(e) {
					console.log(e)
				}
            });
        }
	},
	components: {
		QrcodeVue,
	},
	mounted() {
		this.fetchTags()
	}
}