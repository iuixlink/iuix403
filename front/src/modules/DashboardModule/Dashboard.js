import QrcodeVue from 'qrcode.vue'
import httpAxios from '@/httpAxios.js'

export default {
    name: 'Dashboard-Module',
	metaInfo: function() {
		return {
			title: this.$t('navbar.dashboard'),
		}
	},
	data: () => {
		return { 
			tags: []
		}
	}, 
	methods: {
		async fetchTags() {
			const response = await httpAxios({
				url: '/tags',
				method: 'GET'
			});
			this.tags = response.data;
		}
	},
	components: {
		QrcodeVue,
	},
	mounted() {
		this.fetchTags()
	}
}