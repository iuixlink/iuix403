import httpAxios from '@/httpAxios.js'

export default {
    name: 'Register-Module',
	metaInfo: function() {
		return {
			title: this.$t('auth.register'),
		}
	},
	data() {
        console.log(this.$route);
        return {
            registerData: {
                first_name: '',
                last_name: '',
                email: '',
                password: '',
                tag: ((this.$route.params.tagId) ? this.$route.params.tagId  : '') + ((this.$route.query.tagPass) ? this.$route.query.tagPass : '')
            },
            errors: {
				first_name: false,
				last_name: false,
				email: false,
				password: false,
                tag: false
			}
        }
    },
    methods: {
        // Store a new user
        register: function() {
            const self = this

            // Clear Errors
            Object.keys(self.errors).forEach(function(key) {
                self.errors[key] = false
            })
            
            // Ajax Request
            httpAxios({
                url: '/register',
                method: 'POST',
                data: self.registerData
            })
			.then(response => {
				Object.keys(self.registerData).forEach(function(key) {
                    self.registerData[key] = ''
                })

                self.$store.commit('LOGGED_USER', response.data);
				httpAxios.defaults.headers.common['Authorization'] = 'Bearer ' + response.data.token;
				self.$router.push({ name: 'adminDashboard' });

                self.$notify({
                    group: 'notify',
                    type: 'success',
                    title: self.$t('general.success'),
                    text: 'Bienvenido!'
                })

                
                //setTimeout(() => self.$router.push({ name: 'login' }), 3000)

                console.log(response)
            })
            .catch(error => {
                try {
                    console.log(error)
					if(error.response.status == 422) {
						for(var errorKey in error.response.data.errors) {
							if(errorKey in self.errors) {
                                self.errors[errorKey] = true
                            }
						}
					}
				} catch(e) {
					console.log(e)
				}
            });
        }
	},
}