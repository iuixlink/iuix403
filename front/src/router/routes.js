// Import Modules
import error404Module from '@/modules/Error404Module'
import loginModule from '@/modules/LoginModule'
import registerModule from '@/modules/RegisterModule'
import dashboardModule from '@/modules/DashboardModule'
import tagModule from '@/modules/TagModule'

export default [
    // Home Page
    { path: '/', redirect: '/login' },

    // Errors
    { path: '*',  component: error404Module },

    // Auth
    { path: '/login:tagId?', name: 'login', component: loginModule, meta: { guest: true } },
    { path: '/register/:tagId?', name: 'register', component: registerModule, meta: { guest: true } },

    // Admin
    { path: '/tags/:id', name: 'tag', component: tagModule, meta: { auth: true } },
    { path: '/tags', name: 'adminDashboard', component: dashboardModule, meta: { auth: true } },
]